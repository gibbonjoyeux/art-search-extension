//////////////////////////////// BY CACTUSFLUO /////////////////////////////////

"use strict";

window.browser = window.browser || window.chrome || window.msBrowser;

////////////////////////////////////////////////////////////////////////////////
/// DATA
////////////////////////////////////////////////////////////////////////////////

const	DATA = {
	"behance": {
		"field": ["all creative fields", "advertising", "animation", "ar/vr", "architecture", "art direction", "automotive design", "motion graphics", "branding", "motion graphics", "calligraphy", "cartooning", "character design", "cinematography", "computer animation", "copywriting", "costume design", "crafts", "creative direction", "culinary arts", "motion graphics", "digital art", "digital photography", "directing", "drawing", "motion graphics", "editing", "editorial design", "engineering", "entrepreneurship", "exhibition design", "motion graphics", "fashion", "fashion styling", "film", "fine arts", "furniture design", "motion graphics", "game design", "graffiti", "graphic design", "motion graphics", "icon design", "illustration", "industrial design", "information architecture", "interaction design", "interior design", "motion graphics", "jewelry design", "journalism", "motion graphics", "landscape design", "motion graphics", "makeup arts (mua)", "motion graphics", "music", "motion graphics", "packaging", "painting", "pattern design", "performing arts", "photography", "photojournalism", "print design", "product design", "programming", "motion graphics", "retouching", "motion graphics", "sculpting", "set design", "sound design", "storyboarding", "street art", "motion graphics", "textile design", "toy design", "typography", "motion graphics", "ui/ux", "motion graphics", "visual effects", "motion graphics", "web design", "web development", "writing"],
		"mode": [["all results", "all"], "projects", ["people", "users"], "moodboards"],
		"sort": [["curated", "featured_date"], ["most appreciated", "appreciations"], ["most viewed", "views"], ["most discussed", "comments"], ["most recent", "published_date"], ["most relevant", "relevance"]],
		"time": [["all time", "all"], ["this month", "month"], ["this week", "week"], "today"]
	},
	"pinterest": {
		"mode": [["all pins", "pins"], ["your pins", "my_pins"], ["people", "users"], "boards"],
	},
	"instagram": {
		"mode": ["tag", "people"]
	},
	"artstation": {
		"mode": ["artwork", "artist"],
		"sort": ["relevance", "likes", ["latest", "date"], ["trending now", "rank"]]
	},
	"tumblr": {},
	"dribbble": {
		"mode": ["all", "animation", "branding", "illustration", "mobile", "print", ["product design", "product-design"], "typography", ["web design", "web-design"]],
		"sort": ["popular", "teams", "debuts", "recent"]
	},
	"designspiration" : {
		"mode": ["saves", "collections", "people"]
	}
};

let	g_nav				= null;
let	g_selected			= null;
let	g_input_keyword		= null;
let	g_input_field		= null;
let	g_input_mode		= null;
let	g_input_sort		= null;
let	g_input_time		= null;
let	g_input_submit		= null;
let	g_delimiter			= null;

////////////////////////////////////////////////////////////////////////////////
/// FUNCTIONS
////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////
/// SEARCH FUNCTIONS
//////////////////////////////////////////////////

function		search_with_behance(keyword, field, mode, sort, time) {
	let			url;

	url = `https://behance.net/search`;
	url += `?search=${encodeURIComponent(keyword)}`;
	url += `&sort=${sort}`;
	url += `&time=${time}`;
	if (mode != "all") {
		url += "&content=" + mode;
	}
	if (field != "all creative fields") {
		url += "&field=" + encodeURIComponent(field);
	}
	return (url);
}

function		search_with_pinterest(keyword, field, mode, sort, time) {
	let			url;

	url = `https://pinterest.com/search/${mode}`;
	url += `?q=${encodeURIComponent(keyword)}`;
	return (url);
}

function		search_with_instagram(keyword, field, mode, sort, item) {
	if (mode == "tag") {
		return (`https://instagram.com/explore/tags/${keyword}`);
	}
	return (`https://instagram.com/${keyword}`);
}

function		search_with_artstation(keyword, field, mode, sort, item) {
	let			url;

	url = "https://www.artstation.com/search";
	if (mode == "artist") {
		url += "/artist?q="
	} else {
		url += "?q="
	}
	url += encodeURIComponent(keyword) + "&sort_by=" + sort;
	return (url);
}

function		search_with_tumblr(keyword, field, mode, sort, item) {
	return (`https://www.tumblr.com/search/${encodeURIComponent(keyword)}`);
}

function		search_with_dribbble(keyword, field, mode, sort, item) {
	let			url;

	url = `https://dribbble.com/search/shots/${sort}`;
	url += `/${mode}`;
	url += `?q=${encodeURIComponent(keyword)}`;
	return (url);
}

function		search_with_designspiration(keyword, field, mode, sort, item) {
	let			url;

	url = `https://designspiration.com/search/${mode}`;
	url += `/?q=${encodeURIComponent(url)}`;
	return (url);
}

//////////////////////////////////////////////////
/// STORAGE
//////////////////////////////////////////////////

function	save_input_to_storage() {
	let		data;
	let		to_storage;

	data = {};
	if ("field" in DATA[g_selected]) {
		data.field = g_input_field.value;
	}
	if ("mode" in DATA[g_selected]) {
		data.mode = g_input_mode.value;
	}
	if ("sort" in DATA[g_selected]) {
		data.sort = g_input_sort.value;
	}
	if ("time" in DATA[g_selected]) {
		data.time = g_input_time.value;
	}
	to_storage = {};
	to_storage[g_selected] = data;
	browser.storage.local.set(to_storage, null);
}

function	set_input_from_storage(data) {
	data = data[g_selected];
	if (data == null) {
		return;
	}
	if ("field" in data) {
		g_input_field.value = data.field;
	}
	if ("mode" in data) {
		g_input_mode.value = data.mode;
	}
	if ("sort" in data) {
		g_input_sort.value = data.sort;
	}
	if ("time" in data) {
		g_input_time.value = data.time;
	}
}

//////////////////////////////////////////////////
/// MAIN
//////////////////////////////////////////////////

function	clear_select(select, type, site) {
	let		i;

	while (select.options.length > 0) {
		select.options.remove(0);
	}
	if (DATA[site][type] != null) {
		/// WITH MODE
		select.style.display = "block";
		for (i = 0; i < DATA[site][type].length; i++) {
			if (typeof(DATA[site][type][i]) == "string") {
				select.options.add(new Option(DATA[site][type][i]));
			} else {
				select.options.add(new Option(DATA[site][type][i][0]));
			}
		}
	} else {
		/// WITHOUT MODE
		select.style.display = "none";
	}
}

function	clear_search(site) {
	/// FOR EACH SITE
	for (const site in DATA) {
		if (g_selected == site) {
			g_nav[site].setAttribute("class", "nav_site selected");
		} else {
			g_nav[site].setAttribute("class", "nav_site");
		}
	}
	/// CLEAR SELECT INPUTS
	clear_select(g_input_field, "field", site);
	clear_select(g_input_mode, "mode", site);
	clear_select(g_input_sort, "sort", site);
	clear_select(g_input_time, "time", site);
	/// DISPLAY
	if (Object.keys(DATA[site]).length > 0) {
		g_delimiter.style.display = "block";
	} else {
		g_delimiter.style.display = "none";
	}
	g_input_keyword.style.display = "block";
	g_input_submit.style.display = "block";
}

function	get_url_name(type, name) {
	/// IF TYPE EXISTS
	if (DATA[g_selected][type] != null) {
		/// GET CORRESPONDING KEY
		for (const key of DATA[g_selected][type]) {
			if (typeof(key) == "string") {
				if (key == name) {
					return (key);
				}
			} else {
				if (key[0] == name) {
					return (key[1]);
				}
			}
		}
	}
	return (name);
}

function	submit_search() {
	let		field;
	let		mode;
	let		sort;
	let		time;
	let		url;

	save_input_to_storage();
	/// GET CORRESPONDING URL NAMES
	field = get_url_name("field", g_input_field.value);
	mode = get_url_name("mode", g_input_mode.value);
	sort = get_url_name("sort", g_input_sort.value);
	time = get_url_name("time", g_input_time.value);
	/// GET URL
	url = window["search_with_" + g_selected](g_input_keyword.value, field,
	mode, sort, time);
	/// OPEN URL
	browser.tabs.create({ "url": url });
}

document.addEventListener('DOMContentLoaded', function() {
	/// GET ELEMENTS
	g_input_keyword = document.getElementById("input_keyword");
	g_input_field = document.getElementById("input_field");
	g_input_mode = document.getElementById("input_mode");
	g_input_sort = document.getElementById("input_sort");
	g_input_time = document.getElementById("input_time");
	g_input_submit = document.getElementById("input_submit");
	g_delimiter = document.getElementById("search_delimiter");
	/// HANDLE SITE SELECTION
	g_nav = {};
	for (const site in DATA) {
		g_nav[site] = document.getElementById("nav_" + site);
		g_nav[site].addEventListener("click", function () {
			g_selected = site;
			clear_search(site);
			browser.storage.local.get(g_selected, set_input_from_storage);
		});
	}
	/// HANDLE SEARCH SUBMISSION
	document.getElementById("search").addEventListener("submit", submit_search);
});
